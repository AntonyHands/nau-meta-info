/*
 * $Id:$
 */
package ru.naumen.metainfo.metaclassdata;

import java.util.function.Predicate;

import ru.naumen.metainfo.IMetaClass;

/**
 * @author abezrukov
 * @since 6 апр. 2018 г.
 */
public interface IMetaClassAdvLoggingData extends IMetaClassData
{
    /**
     * @return
     */
    boolean isLoggable();

    /**
     * Предикат, проверяющий, что сущности, соответствующие метаклассу, необходимо
     * логировать с помощью AdvLogger
     */
    public static final Predicate<IMetaClass> IS_LOGGABLE = mc -> mc
            .getOptionalData(IMetaClassAdvLoggingData.class).map(d -> d.isLoggable()).orElse(false);
}
