/*
 * $Id:$
 */
package ru.naumen.metainfo.metaclassdata;

import ru.naumen.metainfo.IMetaClass;

/**
 * Дополнительные данные о мета-классе
 * 
 * @author abezrukov
 * @since 27 янв. 2018 г.
 */
public interface IMetaClassData
{
    /**
     * @return Метакласс, к которому относятся текущие данные
     */
    IMetaClass getMetaClass();
}
