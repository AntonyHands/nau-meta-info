/*
 * $Id:$
 */
package ru.naumen.metainfo.metaclassdata;

/**
 * Данные о справочнике, к которому относятся объекты, реализующие текущий метакласс
 * 
 * @author abezrukov
 * @since 7 февр. 2018 г.
 */
public interface IMetaClassCatalogData extends IMetaClassData
{
    /**
     * Возвращает код справочника, к которому относятся объекты, реализующие текущий метакласс
     * 
     * @return
     */
    String getCatalogCode();
}
