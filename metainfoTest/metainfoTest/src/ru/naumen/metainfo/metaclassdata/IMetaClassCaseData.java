/*
 * $Id:$
 */
package ru.naumen.metainfo.metaclassdata;

/**
 * Данные о типе (о категории), к которой относятся объекты, реализующие данный
 * метакласс
 * 
 * @author abezrukov
 * @since 7 февр. 2018 г.
 */
public interface IMetaClassCaseData extends IMetaClassData
{
    /**
     * Возвращает код категории, к которой относятся объекты, реализующие данный
     * метакласс
     * 
     * @return
     */
    String getCaseCode();
}
