/*
 * $Id:$
 */
package ru.naumen.metainfo.metaclassdata;

import java.util.function.Predicate;

import ru.naumen.metainfo.IMetaClass;

/**
 * Интерфейс-признак для того, что сущности, относящиеся к данному метаклассу,
 * необходимо учитывать при настройке пользовательских AdHoc-отчётов.
 * <p>
 * Если эти данные у метакласса есть, то такие сущности так или иначе должны
 * отображаться в AdHoc. Если сущность к отчётам отношения не имеет, этих данных
 * в метаклассе не будет.
 * 
 * @author abezrukov
 * @since 7 февр. 2018 г.
 */
public interface IMetaClassAdHocUIDescrData extends IMetaClassData
{

    /**
     * Признак того, что данный класс нужен в системе отчётности.
     * 
     * @return
     */
    boolean isReportingEntity();

    /**
     * Признак того, что данный класс является связкой между сущностями для
     * отчётной подсистемы
     * 
     * @return
     */
    boolean isReportingEntityRelation();

    /**
     * Предикат, проверяющий, что сущности, соответствующие метаклассу, должны
     * отображаться в настройке отчётов
     */
    public static final Predicate<IMetaClass> IS_REPORTING_ENTITY = mc -> mc
            .getOptionalData(IMetaClassAdHocUIDescrData.class)
            .map(d -> d.isReportingEntity())
            .orElse(false);

    /**
     * Предикат, проверяющий, что сущности, соответствующие метаклассу, являются
     * связями между отчётными сущностями
     */
    public static final Predicate<IMetaClass> IS_REPORTING_ENTITY_RELATION = mc -> mc
            .getOptionalData(IMetaClassAdHocUIDescrData.class)
            .map(d -> d.isReportingEntityRelation())
            .orElse(false);
}
