/*
 * $Id:$
 */
package ru.naumen.metainfo;

import java.util.Optional;

/**
 * Утилитарный класс, осуществляющий доступ к метаклассам
 * 
 * @author abezrukov
 * @since 28 янв. 2018 г.
 */
public final class MetaClassesFacade
{

    /**
     * Получить метакласс по идентификатору.
     * Предположительно, он должен браться из кэша (быстрая операция).
     * 
     * @param id
     * @return
     */
    public static Optional<IMetaClass> getById(String id)
    {
        return null;
    }

    /**
     * Получить метакласс по определённому классу. Если для указанного класса
     * метакласс не определён, пытаемся найти метакласс для родителя указанного
     * класса, и т.д.
     * 
     * @param clazz
     * @return
     */
    public static Optional<IMetaClass> getByClassClosure(Class<?> clazz)
    {
        Class<?> clazz_ = clazz;
        while(true)
        {
            Optional<IMetaClass> mcOpt = getById(clazz_.getName());
            if (mcOpt.isPresent())
                return mcOpt;
            else if (clazz_.getSuperclass() != null)
                clazz_ = clazz_.getSuperclass();
            else
                return Optional.empty();
        }
    }

    /**
     * Получить метакласс объекта
     * 
     * @param object
     * @return
     */
    public static Optional<IMetaClass> getByObject(Object object)
    {
        return null;
    }
}
