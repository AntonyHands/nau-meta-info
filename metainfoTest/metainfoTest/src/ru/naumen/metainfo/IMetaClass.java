/*
 * $Id:$
 */
package ru.naumen.metainfo;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import ru.naumen.metainfo.metaclassdata.IMetaClassAdvLoggingData;
import ru.naumen.metainfo.metaclassdata.IMetaClassData;
import ru.naumen.metainfo.property.IMetaProperty;

/**
 * Метакласс. Описывает некую сущность (представленную Java-классом или
 * Java-интерфейсом) в общем, универсальном виде.
 * <p>
 * Т.к. между метаклассами, соответствующими классам и интерфейсам, не
 * существует разницы, то для них реализовано "массовое наследование".
 * <p>
 * Так как метаинформация не ограничена ничем, кроме фантазии аналитиков и
 * разработчиков, в данном интерфейсе реализованы только базовые, самые
 * необходимые методы её получения (базовые данные о метаклассе и получение
 * метасвойств). Остальное вынесено в "дополнительные данные", которые можно
 * добыть с помощью {@link #getOptionalData(Class)}. Все дополнительные данные о
 * метаклассе реализуют интерфейс {@link IMetaClassData}, и зачастую самый
 * простой способ получить нужную метаинформацию - найти интерфейс-наследник от
 * {@link IMetaClassData}, а в нём - "помощника" (предикат для фильтрации или
 * Function для преобразования, в зависимости от направленности дополнительных
 * метаданных), и воспользоваться этим helper-кодом.
 * 
 * @author abezrukov
 * @since 27 янв. 2018 г.
 */
public interface IMetaClass
{
    /**
     * Получить строковый идентификатор мета-класса.
     * В простейшем случае совпадает с именем класса (Class.getName()).
     * Может после имени класса содержать JSON-строку с дополнительными
     * параметрами.
     * 
     * @return Строковый идентификатор мета-класса
     */
    String getId();

    /**
     * @return Класс сущностей, описываемых текущим метаклассом
     */
    Class<?> getClazz();

    /**
     * Наименование мета-класса
     * 
     * @return
     */
    String getDisplayableTitle();

    /**
     * Получить описание метакласса (пояснение, что за сущности он представляет)
     * 
     * @return Строка (в т.ч. пустая), not-null
     */
    String getDescription();

    /**
     * Получить список всех свойств метакласса
     * 
     * @return
     */
    default List<IMetaProperty> listProperties()
    {
        return listProperties(t -> true);
    }

    /**
     * Получить список свойств метакласса, отфильтрованный с помощью predicate
     * 
     * @param predicate
     * @return
     */
    List<IMetaProperty> listProperties(Predicate<IMetaProperty> predicate);

    /**
     * Получить мета-свойство по его идентификатору (если такое есть)
     * 
     * @param propertyId
     * @return
     */
    Optional<IMetaProperty> getProperty(String propertyId);

    List<IMetaClass> listParentMetaClasses();

    List<IMetaClass> listChildMetaClasses();

    default boolean isAssignableFrom(IMetaClass mc)
    {
        return mc.equals(this)
                || mc.listParentMetaClasses().stream().anyMatch(pmc -> isAssignableFrom(pmc));
    }

    /**
     * Получить необязательные (они могут отсутствовать для текущего метакласса)
     * дополнительные данные о метаклассе по реализуемому ими интерфейсу.
     * <p>
     * Архитектура системы такова, что обычно проще не вызывать этот метод, а
     * воспользоваться "помощниками" в нужном интерфейсе дополнительных данных
     * (наследнике от {@link IMetaClassData}).
     * Например, при необходимости проверить, следует ли логировать изменения над
     * определённой сущностью, проще воспользоваться предикатом
     * {@link IMetaClassAdvLoggingData#IS_LOGGABLE}, который внутри уже вызовет
     * данный метод и правильно обработает случай отсутствия данных.
     * 
     * @param dataInterface
     * @return
     */
    <T extends IMetaClassData> Optional<T> getOptionalData(Class<T> dataInterface);
}
