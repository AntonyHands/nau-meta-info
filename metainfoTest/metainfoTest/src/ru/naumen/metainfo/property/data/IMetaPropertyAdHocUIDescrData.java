/*
 * $Id:$
 */
package ru.naumen.metainfo.property.data;

import java.util.function.Predicate;

import ru.naumen.metainfo.property.IMetaProperty;

/**
 * Данные, определяющие, является ли текущее мета-свойство отображаемым в
 * настройке пользовательских AdHoc-отчётjd.
 * <p>
 * Если этих данных в мета-свойстве нет, то свойство в настройках отчёта
 * отображается.
 * 
 * @author abezrukov
 * @since 7 февр. 2018 г.
 */
public interface IMetaPropertyAdHocUIDescrData extends IMetaPropertyData
{
    /**
     * Является ли текущее мета-свойство используемым в настройке
     * пользовательских AdHoc-отчётов
     * 
     * @return
     */
    boolean isReportingProperty();

    /**
     * Предикат, проверяющий, что текущее мета-свойство должно быть доступно в
     * настройке пользовательских AdHoc-отчётов
     */
    public static final Predicate<IMetaProperty> IS_REPORTING_PROPERTY = metaProperty -> metaProperty
            .getOptionalData(IMetaPropertyAdHocUIDescrData.class)
            .map(IMetaPropertyAdHocUIDescrData::isReportingProperty)
            .orElse(true);
}
