/*
 * $Id:$
 */
package ru.naumen.metainfo.property.data;

import java.util.function.Predicate;

import ru.naumen.metainfo.property.IMetaProperty;

/**
 * Дополнительные данные о мета-свойстве, значение которого является
 * множественным (коллекция или массив).
 * <p>
 * Присутствуют в мета-свойстве только если оно имеет "множественное" значение.
 * 
 * @author abezrukov
 * @since 7 февр. 2018 г.
 */
public interface IMetaPropertyMultiValueData extends IMetaPropertyData
{
    /**
     * Класс одиночного значения мета-свойства, являющегося коллекцией или массивом.
     * 
     * @return
     */
    Class<?> getSingleValueClass();

    /**
     * Предикат, проверяющий, что текущее мета-свойство имеет множественное
     * значение (коллекция или массив)
     */
    public static final Predicate<IMetaProperty> IS_MULTI_VALUE = metaProperty -> metaProperty
            .getOptionalData(IMetaPropertyMultiValueData.class).isPresent();
}
