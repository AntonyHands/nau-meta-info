/*
 * $Id:$
 */
package ru.naumen.metainfo.property.data;

import ru.naumen.metainfo.property.IMetaProperty;

/**
 * Интерфейс, хранящий базовые (применимые во всех случаях) UI-свойства
 * 
 * @author abezrukov
 * @since 27 янв. 2018 г.
 */
public interface IMetaPropertyUIBase
{
    /**
     * Получить идентификатор мета-свойства в UI.
     * <p>
     * Обычно представляет "безопасный" вариант строки {@link IMetaProperty#getId()}
     * 
     * @return
     */
    String getUIName();

    /**
     * Признак, что свойство предназначено только для чтения
     * <p>
     * Такое свойство нельзя редактировать из UI
     * 
     * <p>TODO вероятно, имеет смысл зависеть ещё и от пользователя
     * 
     * @return
     */
    boolean isReadOnly(Object owner);

    /**
     * Признак видимости свойства в UI
     * 
     * <p>TODO вероятно, имеет смысл зависеть ещё и от пользователя
     * 
     * @return
     */
    boolean isVisible(Object owner);
}
