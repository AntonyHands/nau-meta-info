/*
 * $Id:$
 */
package ru.naumen.metainfo.property.data;

/**
 * Данные, предназначенные для отображения мета-свойств в UI Guic.
 * 
 * TODO отображение фильтров может сильно не совпадать с просто полями
 * (промежуток дат в фильтре)
 * 
 * @author abezrukov
 * @since 27 янв. 2018 г.
 */
public interface IMetaPropertyUIGuicData extends IMetaPropertyUIBase, IMetaPropertyMandatoryData
{
    /**
     * Представить значение текущего свойства в виде HTML (в основном,
     * предназначено для отображения в карточках в UI)
     * 
     * @param owner
     * @return
     */
    String getHTMLValue(Object owner);

    /**
     * Добавить поле ввода для текущего мета-свойства на форму
     * добавления/редактирования объекта owner
     * 
     * @param owner
     * @param form
     */
    void customizeInstanceForm(Object owner, Object form);

    /**
     * Получить значение текущего мета-свойства с формы form
     * добавления/редактирования объекта owner
     * 
     * @param owner
     * @param form
     * @return
     */
    Object extractValue(Object owner, Object form);

    /**
     * Получить объект, позволяющий рендерить значение на карточках объектов и
     * поле ввода на формах
     * <p>
     * TODO У нас есть "синтаксический сахар" в виде методов выше, которые
     * пользуются этим методом. Получается "масло масляное".
     * 
     * @return
     */
    Object getPresentation();

    @Override
    default String getUIName()
    {
        // точки в идентификаторах компонент интерфейса запрещены - превращаем
        // их в подчёркивания
        return getMetaProperty().getId().replaceAll("\\.", "_"); //$NON-NLS-1$ //$NON-NLS-2$
    }
}
