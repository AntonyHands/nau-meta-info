/*
 * $Id:$
 */
package ru.naumen.metainfo.property.data;

import ru.naumen.metainfo.property.IMetaProperty;

/**
 * Интерфейс-маркер для дополнительных (необязательных) данных о мета-свойстве
 * 
 * @author abezrukov
 * @since 27 янв. 2018 г.
 */
public interface IMetaPropertyData
{
    /**
     * @return Мета-свойство, к которому относятся дополнительные данные
     */
    IMetaProperty getMetaProperty();
}
