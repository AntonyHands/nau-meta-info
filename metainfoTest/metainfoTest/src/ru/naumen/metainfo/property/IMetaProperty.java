/*
 * $Id:$
 */
package ru.naumen.metainfo.property;

import java.io.Serializable;
import java.util.Optional;

import ru.naumen.metainfo.IMetaClass;
import ru.naumen.metainfo.property.data.IMetaPropertyData;
import ru.naumen.metainfo.property.data.IMetaPropertyMandatoryData;
import ru.naumen.metainfo.property.data.IMetaPropertyMultiValueData;

/**
 * Интерфейс мета-свойства
 * <p>
 * 
 * @author abezrukov
 * @since 27 янв. 2018 г.
 */
public interface IMetaProperty
{
    /**
     * @return Идентификатор мета-свойства
     */
    String getId();

    /**
     * @return Наименование мета-свойства
     */
    String getDisplayableTitle();

    /**
     * Возвращает описание мета-свойства (хелп-текст, который можно показать,
     * например, на форме ввода этого свойства)
     * 
     * @return Описание мета-свойства (строка, в т.ч. может быть пустая, но не
     *         null)
     */
    String getDescription();

    /**
     * Получить мета-класс, к которому относится данное мета-свойство
     * 
     * @return
     */
    IMetaClass getMetaClass();

    /**
     * Возвращает класс значений свойства.
     * <p>
     * Для массивов и коллекций возвращаются, соответственно, классы массивов и
     * коллекций; и чтобы получить класс одиночного объекта коллекции следует
     * воспользоваться {@link IMetaPropertyMultiValueData#getSingleValueClass()}
     * 
     * @return
     */
    Class<?> getValueClass();

    /**
     * Возвращает метакласс значений свойства. Причем для коллекций возвращается
     * метакласс элемента коллекции
     * 
     * @return
     */
    IMetaClass getValueMetaClass();

    /**
     * Получить значение атрибута из объекта-владельца атрибута
     * 
     * @param owner
     *            - объект-владелец атрибута
     * @return Значение атрибута
     */
    Object getValue(Object owner);

    /**
     * Получить сериализуемое значение атрибута из объекта-владельца атрибута.
     * <p>
     * Данный метод имеет 2 особенности:
     * <nl>
     * <li>1. Если значение атрибута сериализуемое, возвращает его. Если нет -
     * возвращает враппер (обычно {@link UniversalWrapper} или его наследника)
     * над объектом-значением (или коллекцию/массив соответствующих врапперов).
     * </li>
     * <li>2. Данный метод должен быть оптимизирован для работы в списках, т.е.
     * отрабатывать максимально быстро на больших объёмах объектов-владельцев.
     * </li>
     * </nl>
     * 
     * @param owner
     * @return
     */
    Serializable getSerializableValue(Object owner);

    /**
     * В рамках сессии session присвоить свойству объекта owner значение value
     * <p>
     * TODO Решить, нужно ли точное соответствие value, или можно передавать что
     * угодно, а здесь его уже приводить к нужному значению. Пока склоняюсь к
     * тому, что приведение значения нужно выполнять внутри данного метода,
     * чтобы можно было откуда угодно вызывать его, не заморачиваясь за
     * обязательность типизации.
     * 
     * @param session
     * @param owner
     * @param value
     */
    void setValue(Object session, Object owner, Object value);

    /**
     * Получить данные, предоставленные одним из расширений метаинфы.
     * <p>
     * Данный может не быть (например, данными о справочнике обладают только
     * элементы справочника)
     * 
     * @param dataInterface
     * @return
     */
    <T extends IMetaPropertyData> Optional<T> getOptionalData(Class<T> dataInterface);

    /**
     * Получить данные, предоставленные одним из обязательных расширений
     * метаинфы.
     * <p>
     * Является "синтаксическим сахаром" для метода
     * {@link IMetaProperty#getOptionalData(Class)}, позволяя обходиться без
     * работы с {@link Optional}
     * 
     * @param mandatoryDataInterface
     * @return
     */
    default <T extends IMetaPropertyMandatoryData> T getData(Class<T> mandatoryDataInterface)
    {
        return getOptionalData(mandatoryDataInterface).get();
    }
}
